import { REQUEST_EVENTS, RECEIVE_EVENTS, ERROR } from '../constants/action-types';

const initialState = {
  isFetching: false,
  data: {
    events: []
  },
  searchTerm: ''
};

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case REQUEST_EVENTS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_EVENTS:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.data,
        searchTerm: action.searchTerm,
        lastUpdated: action.receivedAt
      });
    case ERROR:
      return state;

    default:
      return state;
  }
};

export default reducer;
