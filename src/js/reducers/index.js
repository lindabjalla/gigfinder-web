import { combineReducers } from 'redux';
import eventsReducer from './search-result';

const reducer = combineReducers({
  eventsReducer
});

export default reducer;
