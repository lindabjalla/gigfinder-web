import * as types from '../constants/action-types';
import axios from 'axios';

export const receiveEvents = (data, query) => ({
  type: types.RECEIVE_EVENTS,
  data: data,
  searchTerm: query.searchInput,
  receivedAt: Date.now()
});

const internalError = errorText => ({
  type: types.ERROR,
  data: errorText
});

const getEventUri = query => {
  if (query.searchInput && query.searchInput.length > 0) {
    return '/gig-finder/api/v1/event/city/' + query.searchInput;
  } else {
    return '/gig-finder/api/v1/event/';
  }
};

export const requestEvents = query => dispatch => {
  //http://localhost:8080/api/gig-finder/event/city/Antwerp?page=1&results=10
  axios.get(getEventUri(query), {
    params: {
      page: query.page,
      results: query.results
    }
  }).then((response) => {

    dispatch(receiveEvents(response.data, query));

  }).catch((error) => {

    dispatch(internalError(error.message));

  });
};
