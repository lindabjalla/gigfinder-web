import React from 'react';
import PropTypes from 'prop-types';
import { ListGroup } from 'react-bootstrap';
import { SPACE, UNKNOWN_LOCATION } from '../constants/app';

const MusicEvents = (props) => {

  const renderNameAndLinkIfHasWebsite = (something, index, array) => {

    const isLast = (index === array.length - 1);

    const commaOrNone = isLast ? '' : ', ';

    if (something.website) {
      return (
        <span key={something.id}>
          <a href={something.website} target="_blank"> {something.name + commaOrNone} </a>
        </span>
      );
    } else {
      return (<span key={something.id}> {something.name + commaOrNone} </span>);
    }
  };

  const artistContainer = (musicEvent) => {

    return (
      <div>
        <label>Artist:
          {musicEvent.artists.map(renderNameAndLinkIfHasWebsite)}
        </label>

      </div>
    );
  };

  const timeContainer = (event) => {
    if (event.startTime.time) {
      return (
        <div>
          <label>Time:
            <span> {event.startTime.time}</span>
          </label>
        </div>
      );
    }
  };

  const location = event =>
    event.venue.area ? SPACE + event.venue.area.city + ', ' + event.venue.area.country : SPACE + UNKNOWN_LOCATION;

  const venue = event => event.venue.website ?
    <a href={event.venue.website} target="_blank">{SPACE + event.venue.name}</a> : SPACE + event.venue.name;

  const listItems = props.data.events.map((event) =>
    <li key={event.id} className="list-group-item">
      <h3>{event.name}</h3>
      {artistContainer(event)}
      <div>
        <label>Date:
          <span> {event.startTime.date}</span>
        </label>
      </div>
      {timeContainer(event)}
      <div>
        <label>Location:
          <span>{location(event)}</span>
        </label>
      </div>
      <div>
        <label>Venue:
          <span>{venue(event)}</span>
        </label>
      </div>
    </li>
  );

  return (
    <ListGroup>{listItems}</ListGroup>
  );
};

MusicEvents.propTypes = {
  events: PropTypes.array,
  data: PropTypes.object,
  website: PropTypes.string,
  artists: PropTypes.array,
  venue: PropTypes.object,
  city: PropTypes.string,
  country: PropTypes.string
};

export default MusicEvents;
