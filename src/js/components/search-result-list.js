import React from 'react';
import PropTypes from 'prop-types';
import { Pagination } from 'react-bootstrap';
import MusicEvents from './musicevents-resultcontainer';

const PaginationAdvanced = props => {

  return (
    <Pagination
      prev
      next
      first
      last
      ellipsis
      boundaryLinks
      items={Math.ceil(props.data.hits / props.numberOfHitsPerPage)}
      maxButtons={5}
      activePage={props.data.pageNumber}
      onSelect={props.callback}
    />
  );
};

PaginationAdvanced.propTypes = {
  data: PropTypes.object,
  handlePaginationChange: PropTypes.func,
  callback: PropTypes.func,
  pageNumber: PropTypes.number,
  numberOfHitsPerPage: PropTypes.number
};

const SearchResultList = props => {

  let searchInput = props.searchResult.searchTerm;
  let searchResultContainer = null;

  const handlePaginationChange = page => {
    fetchEvents(page);
    searchResultContainer.scrollIntoView();
  };

  const fetchEvents = page => {
    const query = {
      page: page,
      results: props.numberOfHitsPerPage,
      searchInput: searchInput
    };
    props.handleRequestEvents(query);
  };

  const result = () => {
    if (props.searchResult.data.hits === undefined) {
      return (
        <div>
          <p>&nbsp;&nbsp;*</p>
          <p>&nbsp;** </p>
          <p>***</p>
        </div>
      );
    } else if (props.searchResult.data.hits === 0) {
      return (
        <section>
          <p>no hits for {searchInput}!</p>
        </section>);
    } else {
      return (
        <section ref={elem => searchResultContainer = (elem)}>
          <h3>{props.searchResult.isFetching}</h3>
          <div className="result-info-text">
            <label>Showing number</label>
            {props.searchResult.data.rangeStart} to {props.searchResult.data.rangeEnd} of
            total {props.searchResult.data.hits + ' '} in {props.searchResult.searchTerm
            ? props.searchResult.searchTerm : 'the whole wide world!'}
          </div>
          <PaginationAdvanced
            data={props.searchResult.data}
            callback={handlePaginationChange}
            numberOfHitsPerPage={props.numberOfHitsPerPage}
          />
          <MusicEvents data={props.searchResult.data}/>
          <PaginationAdvanced
            data={props.searchResult.data}
            callback={handlePaginationChange}
            numberOfHitsPerPage={props.numberOfHitsPerPage}
          />
        </section>
      );
    }
  };

  return (
    <div className="search-result-container">
      {result()}
    </div>
  );
};

SearchResultList.propTypes = {
  searchResult: PropTypes.object,
  numberOfHitsPerPage: PropTypes.number,
  handleRequestEvents: PropTypes.func
};

export default SearchResultList;
