import React from 'react';
import PropTypes from 'prop-types';
import { ControlLabel, FormControl, FormGroup, Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as actions from '../actions/index';
import { SPACE } from '../constants/app';
import SearchResultList from './search-result-list';

const SearchContainer = props => {
  let searchInput = props.searchResult.searchTerm;
  const firstPage = 1;
  const numberOfHitsPerPage = 10;

  const fetchEventsButtonClicked = () => {
    fetchEvents(firstPage);
  };

  const fetchEvents = page => {
    const query = {
      page: page,
      results: numberOfHitsPerPage,
      searchInput: searchInput
    };
    props.handleRequestEvents(query);
  };

  const handleChange = e => {
    searchInput = e.target.value;
  };

  const handleFormSubmit = e => {
    e.preventDefault();
    fetchEvents(firstPage);
  };

  return (
    <div>
      <div className="find-gigs-form">
        <Form inline onSubmit={handleFormSubmit}>
          <FormGroup>
            <ControlLabel>Location:</ControlLabel>
            {SPACE}
            <FormControl
              placeholder="Enter city, country or area"
              type="text"
              onChange={handleChange}
              id="main-search-input"
            />
          </FormGroup>
          {SPACE}
          <Button onClick={() => fetchEventsButtonClicked()}>
            {props.searchResult.searchTerm ? 'Find gigs!' : 'All gigs!'}
          </Button>
        </Form>
      </div>
      <hr/>
      <SearchResultList
        {...props}
        numberOfHitsPerPage={numberOfHitsPerPage}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  searchResult: state.eventsReducer
});

const mapDispatchToProps = dispatch => ({
  handleRequestEvents: (searchQuery) => {
    dispatch(actions.requestEvents(searchQuery));
  }
});

SearchContainer.propTypes = {
  searchTerm: PropTypes.string,
  searchResult: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer);
