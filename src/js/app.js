import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import SearchContainer from './components/searchcontainer';

const myApp =
  <Provider store={store}>
    <SearchContainer />
  </Provider>;

ReactDOM.render(
  myApp, document.getElementById('app')
);
